#!/bin/sh

gen_pod_yaml()
{
  local POD_FILE=$1
  local POD_NAME=$2
  local WORKER_NUM=$3

  pod_template $POD_NAME > $POD_FILE

  if [ $WORKER_NUM -ge 1 ]; then
    for n in `seq 1 $WORKER_NUM`
    do
      pod_worker_template $n >> $POD_FILE
    done
  fi
}

pod_template()
{
  local POD_NAME=$1

  cat << EOS
apiVersion: v1
kind: Pod
metadata:
  name: $POD_NAME
spec:
  restartPolicy: Never
  volumes:
    - name: app
      gitRepo:
        repository: https://monochromegane@bitbucket.org/monochromegane/rails-k8s-sample.git
        revision: master
    - name: vendor-bundle
      hostPath:
        path: /tmp/bundle
  containers:
    - name: mysql
      image: mysql:5.6
      env:
        - name: MYSQL_ROOT_PASSWORD
          value: 'password'
    - name: memcached
      image: memcached:1.4
    - name: app-m
      image: ruby:2.2
      command: ['/bin/sh', '-c']
      args: ['sleep 10 && cd /tmp/app/rails-k8s-sample && sh bin/ci/run.sh']
      volumeMounts:
        - mountPath: /tmp/app
          name: app
        - mountPath: /tmp/vendor/bundle
          name: vendor-bundle
      env:
        - name: RAILS_ENV
          value: test
        - name: MYSQL_TCP_ADDR
          value: '127.0.0.1'
        - name: MYSQL_ROOT_PASSWORD
          value: 'password'
        - name: TEST_HOST_ID
          value: 'm'
        - name: TEST_QUEUE_SOCKET
          value: localhost:12345
        - name: TEST_QUEUE_RELAY_TOKEN
          value: '123'
EOS
}

pod_worker_template()
{
  local WORKER_NUM=$1

  cat << EOS
    - name: app-w${WORKER_NUM}
      image: ruby:2.2
      command: ['/bin/sh', '-c']
      args: ['sleep 10 && cd /tmp/app/rails-k8s-sample && sleep 5 && sh bin/ci/run.sh']
      volumeMounts:
        - mountPath: /tmp/app
          name: app
        - mountPath: /tmp/vendor/bundle
          name: vendor-bundle
      env:
        - name: RAILS_ENV
          value: test
        - name: MYSQL_TCP_ADDR
          value: '127.0.0.1'
        - name: MYSQL_ROOT_PASSWORD
          value: 'password'
        - name: TEST_HOST_ID
          value: 'w${WORKER_NUM}'
        - name: TEST_QUEUE_RELAY
          value: localhost:12345
        - name: TEST_QUEUE_RELAY_TOKEN
          value: '123'
EOS
}

is_terminated_container()
{
  local CONTAINER_NAME=$1
  local POD_STATE="$2"

  local RESULT=$(echo "$POD_STATE" | jq ".status.containerStatuses[] | select(.name == \"${CONTAINER_NAME}\").state | has(\"terminated\")" 2> /dev/null)
  if [ $? -eq 0 -a "$RESULT" = "true" ]; then
    return 0
  else
    return 1
  fi
}

exit_code_for_container()
{
  local CONTAINER_NAME=$1
  local POD_STATE=$2

  local RESULT=`echo "$POD_STATE" | jq ".status.containerStatuses[] | select(.name == \"${CONTAINER_NAME}\").state.terminated.exitCode"`
  return $RESULT
}
