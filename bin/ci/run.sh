#!/bin/sh
bundle install --path=/tmp/vendor/bundle
bundle exec rake db:setup
bundle exec bin/rspec-queue spec
