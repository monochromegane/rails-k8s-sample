#!/bin/sh

help()
{
  local CMDNAME=`basename $0`
  echo "Usage: $CMDNAME -p <PR_NUMBER> -c <COMMIT_NUMBER> [-w <WORKER_NUMBER>] [-s <SERVER_URL>]" 1>&2
}

CMDBASE=`dirname $0`
. ${CMDBASE}/run_rspec_on_k8s_func.sh

MAIN_CONTAINER='app-m'

while getopts p:c:w:s: OPT
do
  case $OPT in
    "p" ) FLG_PR="TRUE";          PR="$OPTARG" ;;
    "c" ) FLG_COMMIT="TRUE" ;     COMMIT="$OPTARG" ;;
    "w" ) FLG_WORKER_NUM="TRUE" ; WORKER_NUM="$OPTARG" ;;
    "s" ) FLG_SERVER_URL="TRUE";  SERVER_URL="$OPTARG" ;;
      * ) help
          exit 1 ;;
  esac
done

if [ "$FLG_PR" != "TRUE" -o "$FLG_COMMIT" != "TRUE" ]; then
  help
  exit 1
fi

POD_NAME="rspec-${PR}-${COMMIT}"
if [ "$FLG_WORKER_NUM" != "TRUE" ]; then
  WORKER_NUM=0
fi
if [ "$FLG_SERVER_URL" = "TRUE" ]; then
  SERVER_URL_OPT="-s $SERVER_URL"
fi

# Create pod
echo "Creating RSpec pod [${POD_NAME}] with ${WORKER_NUM} workers..."

POD_FILE="rspec_on_k8s_pod.yaml"
gen_pod_yaml $POD_FILE $POD_NAME $WORKER_NUM
kubectl $SERVER_URL_OPT create -f $POD_FILE

if [ $? -ne 0 ]; then
  exit 1
fi

# Wait pod
echo "Waiting RSpec pod [${POD_NAME}]..."

for n in `seq 1 30`
do
  POD_STATE=`kubectl $SERVER_URL_OPT get pods $POD_NAME -o json`
  is_terminated_container $MAIN_CONTAINER "$POD_STATE"
  if [ $? -eq 0 ]; then
    exit_code_for_container $MAIN_CONTAINER "$POD_STATE"
    POD_EXIT_CODE=$?
    echo "Exited with ${POD_EXIT_CODE} RSpec pod [${POD_NAME}]."
    break
  fi

  /bin/echo -n '.'
  sleep 5
done

# Show log
kubectl $SERVER_URL_OPT logs $POD_NAME $MAIN_CONTAINER

# Delete pod
kubectl $SERVER_URL_OPT delete pods $POD_NAME

exit $POD_EXIT_CODE
